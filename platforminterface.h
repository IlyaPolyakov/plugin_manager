#ifndef PLATFORMINTERFACE_H
#define PLATFORMINTERFACE_H

#include "command.h"

class PlatformInterface
{
public:
    void sendCommand(const Command& cmd);
//    ~PlatformInterface(){}
};

#endif // PLATFORMINTERFACE_H
