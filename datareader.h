#ifndef DATAREADER_H
#define DATAREADER_H

#include "command.h"
#include "plugin.h"
#include <QDebug>

class DataReader : public Plugin
{
public:
    explicit DataReader(QObject *parent = nullptr);
    void setParams(const QVariant& params) override {}
    QVector<CommandHandler> init() override;
    void start() override {}
};

#endif // DATAREADER_H
