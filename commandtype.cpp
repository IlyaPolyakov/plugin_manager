#include"commandtype.h"

#include <QDataStream>

CommandType::CommandType() {}

CommandType::CommandType(Operator o, const QString& oprnd):
    _operator{o}, _operand{oprnd} {}

void CommandType::set_operator(unsigned int& _operatorSet)
{
    _operator = static_cast<Operator>(_operatorSet);
}

void CommandType::set_operand(QString& _operandSet)
{
    _operand = _operandSet;
}

unsigned int CommandType::get_operator() const
{
    return static_cast<unsigned int>(_operator);
}

QString CommandType::get_operand() const
{
    return _operand;
}

QDataStream& operator>>(QDataStream& str, CommandType& type)
{
    unsigned int _operatorSet;
    QString _operandSet;
    str >> _operatorSet >> _operandSet;
    type.set_operator(_operatorSet);
    type.set_operand(_operandSet);
    return str;
}

QDataStream& operator<<(QDataStream& str, const CommandType& type)
{
    str << type.get_operator() << type.get_operand();
    return str;
}
