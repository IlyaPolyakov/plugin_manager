#include "datareader.h"
#include "datagenerator.h"
#include "pluginmanager.h"

#include <QCoreApplication>
#include <QObject>

void testDataReader()
{
    // Data reader
    DataReader dataReader;
    QVector<CommandHandler> handlers = dataReader.init();
    CommandHandler handler = handlers[0];
    Command cmd({CommandType::INSERT, "Tag"}, QVariant("Hello world"));
    handler.h(cmd);
}

void testDataGenerator()
{
    DataGenerator dataGenerator;
    dataGenerator.setParams(QVariant(QList<QVariant>({1, 5, 1, 5})));
    dataGenerator.start();
//    QObject::connect(&dataGenerator, &DataGenerator::sendCommand, [](const Command& cmd){
//        qDebug() << cmd.getData();
//    });
}
/*
void testPluginManager()
{
    PluginManager pm("settings.ini");
}
*/

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

//    testDataReader();
    testDataGenerator();
    //    testPluginManager();

    return a.exec();
}
