#include "pluginmanager.h"

PluginManager::PluginManager(const QString& settingsPath, QObject *parent) :
    QObject(parent)
{
    loadPlugins(settingsPath);
}

void PluginManager::incomingCommand(const Command& cmd)
{
    qDebug() << __func__;
    // TODO
}

void PluginManager::loadPlugins(const QString& settingsPath)
{
    QSettings settings(settingsPath, QSettings::IniFormat);
    qDebug() << "Settings path:" << settings.fileName();

    int numbOfPlugins = settings.beginReadArray("plugins");
    QPointer<Pugin> plugPtr;
    QString plugName;
    for (int i = 0; i < numbOfPlugins; ++i) {
        settings.setArrayIndex(i);
        plugName = settings.value("name").toString();
        plugPtr = getPlugin(plugName);
        if (!plugPtr)
        {
            qDebug() << "Failed to load plugin" << plugName;
            continue;
        }
        plugName->setParams(settings.value("params"));
        m_plugins.push_back(plugName);
    }

    settings.endArray();
}

QPointer<Plugin> PluginManager::getPlugin(const QString& name)
{
    // TODO: use QPluginLoader later
    static const QVector<QString> k_pluginsNames = {
        "DataReader",
        "DataGenerator"
    };
    if (name == k_pluginsNames[0]) return new DataReader();
    if (name == k_pluginsNames[1]) return new Datagenerator();
    return nullptr;
}
