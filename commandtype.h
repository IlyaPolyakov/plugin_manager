#ifndef COMMANDTYPE_H
#define COMMANDTYPE_H
#include <QCoreApplication>

class CommandType {
public:
    enum Operator
    {
        INSERT,
        UPDATE,
        SELECT,
        DELETE,
        EXECUTE
    };
    CommandType();
    CommandType(Operator o, const QString& oprnd);
    void set_operator(unsigned int& _operatorSet);
    void set_operand(QString& _operandSet);
    unsigned int get_operator() const;
    QString get_operand() const;
private:
    Operator _operator;
    QString _operand;

    friend QDataStream& operator>>(QDataStream& str, CommandType& type);
    friend QDataStream& operator<<(QDataStream& str, const CommandType& type);
};
// регистрируем тип в QVariant
//Q_DECLARE_METATYPE(CommandType);

//где-то в приложении должны быть вызваны до того, как будет установлено первое соединение:

// Чтобы использовать тип T в подключенных к очереди сигнальных и слот-соединениях
//int i = qRegisterMetaType<CommandType>();
// Регистрирует операторы потока для типа. Используются при потоковой передаче QVariant
//qRegisterMetaTypeStreamOperators<CommandType>("CommandType"); // пока ругается
#endif // COMMANDTYPE_H
