#ifndef PLUGINMANAGER_H
#define PLUGINMANAGER_H

#include "command.h"
#include "plugin.h"
#include "datareader.h"
#include "datagenerator.h"

#include <QCoreApplication>
#include <QDebug>
#include <QObject>
#include <QSettings>

class PluginManager : public QObject
{
    Q_OBJECT
private:
    QVector<QPointer<Plugin>> m_plugins;
    QMultiHash<CommandType, QPointer<Plugin>> m_pluginsHash;
    void loadPlugins(const QString& settingsPath);
    QPointer<Plugin> getPlugin(const QString& name, const QVariant& params);

public:
    explicit PluginManager(const QString& settingsPath, QObject *parent = nullptr);

signals:
    void sendCommand(const Command& cmd);

public slots:
    void incomingCommand(const Command& cmd);
};

#endif // PLUGINMANAGER_H
